<?php


$bdd = new PDO('mysql:host=localhost;dbname=childcare;charset=utf8', 'root', '');

$email = $_GET['email'];
$cle = $_GET['cle'];


$stmt = $bdd->prepare("SELECT verif,valid FROM user WHERE email like :email ");
if($stmt->execute(array(':email' => $email)) && $row = $stmt->fetch())
{
    $clebdd = $row['verif'];
    $valid = $row['valid'];
}

if($valid == '1')
{
    echo "Votre compte est déjà actif !";
}
else
{
    if($cle == $clebdd)
    {

        echo "Votre compte a bien été activé !";


        $stmt = $bdd->prepare("UPDATE user SET valid= 1 WHERE email like :email ");
        $stmt->bindParam(':email', $email);
        $stmt->execute();
    }
    else
    {
        echo "Erreur ! Votre compte ne peut être activé...";
    }
}