<?php


$bdd = new PDO('mysql:host=localhost;dbname=childcare;charset=utf8', 'root', '');



$name = htmlspecialchars(trim($_POST['name']));
$firstname = htmlspecialchars(trim($_POST['firstname']));
$email = htmlspecialchars(trim($_POST['email']));
$options = ['cost' => 12,];
$pwd = htmlspecialchars(trim($_POST['pwd']));
$pass_hache = password_hash($pwd, PASSWORD_BCRYPT, $options);

$verif= random_bytes(5 );
$verif2= bin2hex($verif);


if (empty($name) OR empty($firstname) OR empty($email) OR empty($pwd)){
    echo 'Veuillez remplir tous les champs';
} else {
    $req = $bdd->prepare('INSERT INTO user (id,name,firstname,email,pwd,valid,roles_id,verif) VALUES(NULL, :name,:firstname, :email, :pwd,0,2, :verif)');
    $req->execute(array(
        'name' => $name,
        'firstname' => $firstname,
        'email' => $email,
        'pwd' => $pass_hache,
        'verif' => $verif2));
}

$cle = md5(microtime(TRUE)*100000);

$stmt = $bdd->prepare("UPDATE user SET verif=:verif WHERE email like :email");
$stmt->bindParam(':verif', $cle);
$stmt->bindParam(':email', $email);
$stmt->execute();

$destinataire = $email;
$sujet = "Activer votre compte" ;
$entete = "From: inscription@childcare.com" ;

$message = 'Bienvenue sur ChildCare,
 
Pour activer votre compte, veuillez cliquer sur le lien ci dessous
ou copier/coller dans votre navigateur internet.
 

http://localhost/inscri_us/activation.php?email='.urlencode($email).'&cle='.urlencode($cle).'

---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';


mail($destinataire, $sujet, $message, $entete) ;

echo '<a href="index.php">Retour à l\'inscription</a>';
