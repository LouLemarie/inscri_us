<?php


$bdd = new PDO('mysql:host=localhost;dbname=childcare;charset=utf8', 'root', '');
$email= $_POST['email'];


$req = $bdd->prepare('SELECT id, pwd FROM user WHERE email = :email');
$req->execute(array(
    'email' => $email));
$resultat = $req->fetch();


$isPasswordCorrect = password_verify($_POST['pwd'], $resultat['pwd']);

if (!$resultat)
{
    echo 'Mauvais identifiant ou mot de passe !';
}
else
{
    if ($isPasswordCorrect) {
        session_start();
        $_SESSION['id'] = $resultat['id'];
        $_SESSION['email'] = $email;
        echo 'Vous êtes connecté !';
    }
    else {
        echo 'Mauvais identifiant ou mot de passe !';
    }
}